#!/usr/bin/env python3

import sys, subprocess, os, time, string

import src.vector_calcs as vc
from src.pubchem_api import pubchem_api_call
import src.FileParser as Parser
import src.colorter as cter
import argparse
import random
import traceback
from src.datastructures import AtomAVLTree


import numpy as np
from vispy import app, visuals, scene, geometry
from vispy.visuals.transforms import STTransform
from vispy.io import load_data_file
from vispy.io.image import imread
import open3d as o3d

class Molecule:
    def __init__(self, inputfile:str, parser:object) -> None:
        self._parser     = parser
        self._name       = self._parser.name()
        self._atoms      = self._parser.atoms()
        self._atom_count = self._parser.atom_count()
        self._bonds      = {}

        for bond in self._parser.bonds():
            self._bonds[bond] = {}
            coords            = self._parser.bonds()[bond]["coords"]

            self._bonds[bond]["type"]     = self._parser.bonds()[bond]["type"]
            self._bonds[bond]["coords"]   = coords
            self._bonds[bond]["angle"]    = vc.calc_vec_angel(coords[0], coords[1])
            self._bonds[bond]["distance"] = vc.calc_vec_distance(coords[0], coords[1])

    def atom_attrs(self, atom) -> str:
        tsv = Parser.TSV("data/atom/atom-data.tsv", "symbol")
        atom_info = tsv.data()[atom]

        return atom_info

    def name(self) -> str:
        return self._name

    def atoms(self) -> dict:
        return self._atoms

    def atom_count(self) -> int:
        return self._atom_count

    def bonds(self) -> dict:
        return self._bonds

    def __str__(self) -> str:
        return self._parser.__str__()


class MoleculeRenderer:
    def __init__(self, inputfile:str, args:object, parser:object):
        self._molecule = Molecule(inputfile, parser)
        print(self._molecule.__str__())

        mode  = "2d" if args.planar_view else "3d"

        self._types = {
            1: [0],
            2: [0.125, -0.25],
            3: [0.25, -0.25, -0.25],
            4: [0.3, -0.2, -0.2, -0.2]
        }

        atoms, central_vec = self.centralize_molecule(self._molecule.atoms())
        bonds = self.centeralize_bond_vecs(self._molecule.bonds(), central_vec)
        self.vispy_setup(mode)

        if mode == "3d":
            if not args.sticky:
                for a in atoms:
                    self.render_atom_spheres_3d(atoms[a][0], atoms[a][1])
            for b in bonds:
                self.render_atom_bonds_3d(b, bonds[b]["coords"], int(bonds[b]["type"]))
        else:
            self.molecule_2d(atoms, bonds)

        self._canvas.show()
        if sys.flags != 1:
            app.run()

    def centralize_molecule(self, atoms:dict) -> tuple:
        sum_x, sum_y, sum_z = 0, 0, 0
        for a in atoms:
            a_coords = atoms[a][1:]
            sum_x += float(a_coords[0])
            sum_y += float(a_coords[1])
            sum_z += float(a_coords[2])

        n = len(atoms)
        central_vec = [sum_x/n, sum_y/n, sum_z/n]

        for a in atoms:
            atom_coords = np.mat([float(coord) for coord in atoms[a][1:]])
            atom_coords = atom_coords - central_vec
            atoms[a][1:] = atom_coords.tolist()

        return atoms, central_vec

    def centeralize_bond_vecs(self, bonds: list, central_vec: list) -> list:
        for b in bonds:
            coords = bonds[b]["coords"]
            new_coords = []

            for i in range(len(coords)):
                bond_coords = np.mat([float(coord) for coord in coords[i]])
                bond_coords = bond_coords - central_vec
                new_coords.append(bond_coords.tolist()[0])

            bonds[b]["coords"] = new_coords

        return bonds

    def vispy_setup(self, mode:str) -> None:
        self._canvas = scene.SceneCanvas(keys="interactive", title=self._molecule.name(), show=True, size=(800, 600))

        self._view                 = self._canvas.central_widget.add_view()
        self._view.camera.fov      = 100
        self._view.camera.distance = 1000

        if mode == "3d":
            self._view.camera          = "turntable"
            ax_post = np.array([[-1, 0, 0],
                                [1, 0, 0],
                                [0, -1, 0],
                                [0, 1, 0],
                                [0, 0, -1],
                                [0, 0, 1]])

            self._axis = scene.visuals.XYZAxis(pos=ax_post)
            self._view.add(self._axis)

        elif mode == "2d":
            self._view.camera = "panzoom"

    def format_filename(self, name):
        valid_chars = "-_. %s%s" % (string.ascii_letters, string.digits)
        filename    = ''.join(c for c in name if c in valid_chars)
        filename    = filename.replace(' ', '_')  # I don't like spaces in filenames.
        return filename

    def molecule_2d(self, atoms:list, bonds:list) -> None:
        r_term_args = [self._molecule.atom_count()]

        symbols, coords, hex_cols, r_bonds = [], [], [], []

        for a in atoms:
            symbols.append(atoms[a][0])
            hex_cols.append(self._molecule.atom_attrs(atoms[a][0])["cpkHexColor"])
            coords.extend(atoms[a][1][:2])

        for b in bonds:
            r_bonds.extend(bonds[b]["coords"][0][:2])
            r_bonds.extend(bonds[b]["coords"][1][:2])
        r_term_args.append(len(r_bonds))

        filename = " img/molecule_2d/" + self.format_filename(self._molecule.name()) + ".png"
        r_term_args   += r_bonds + symbols + coords + hex_cols
        r_term_args   = " ".join([str(arg) for arg in r_term_args]) + filename
        r_script_name = "Rscript " + os.path.abspath(os.getcwd())
        r_script      = r_script_name + "/src/create_molecule_2d_plot.r " + r_term_args
        subprocess.Popen(r_script, shell = True)
        time.sleep(2)

        img_data = imread(filename.replace(" ", ""))
        image = scene.visuals.Image(img_data, parent=self._view)

    def render_atom_spheres_3d(self, atom:str, coords:list) -> None:
        atom_data = self._molecule.atom_attrs(atom)

        col = f'#{atom_data["cpkHexColor"]}'
        rad = float(atom_data["atomicRadius"])/200

        a = scene.visuals.Sphere(radius=rad, color=col, shading='smooth')
        a.transform = STTransform(translate=coords)

        self._view.add(a)

    def render_atom_bonds_3d(self, atoms:str, coords:list, type) -> None:
        atoms      = atoms.split(",")[0].split("-")

        if atoms[0] == atoms[1]:
            color = self._molecule.atom_attrs(atoms[0])["cpkHexColor"]
            for i in range(1, type+1):
                coords[0][0] = coords[0][0] - self._types[type][i-1]
                coords[1][0] = coords[1][0] - self._types[type][i-1]
                b     = scene.visuals.Tube(coords, color=f"#{color}",
                                           radius=0.05)
                self._view.add(b)
        else:
            for i in range(1, type+1):
                coords[0][0] = coords[0][0] - self._types[type][i - 1]
                coords[1][0] = coords[1][0] - self._types[type][i - 1]
                center = vc.calc_vec_center(coords[0], coords[1])

                color = self._molecule.atom_attrs(atoms[0])["cpkHexColor"]
                b     = scene.visuals.Tube([coords[0], center],
                                           color=f"#{color}", radius=0.05)
                self._view.add(b)

                color = self._molecule.atom_attrs(atoms[1])["cpkHexColor"]
                b = scene.visuals.Tube([center, coords[1]],
                                       color=f"#{color}", radius=0.05)
                self._view.add(b)


class Protein:
    def __init__(self, inputfile:str, parser:object):
        self._parser = parser

    def atoms(self):
        return self._parser.atoms()

    def name(self):
        return self._parser.name()

    def atom_attrs(self, atom):
        tsv = Parser.TSV("data/atom/atom-data.tsv", "symbol")
        atom_info = tsv.data()[atom]

        return atom_info


class ProteinRenderer:
    def __init__(self, inputfile:str, args:object, parsers:list):
        self._args    = args
        self._protein = Protein(inputfile, parsers[0])
        self.vispy_setup()

        if args.combine:
            self._combined_files  = {} #{file: color}

            for i in range(1, 4):
                if i == 1:  ifile = "inputfile"
                else: ifile = f"self._args.inputfile{i}"

                color = eval(f"self._args.combinecolor{i}")
                if color != None:
                    prot = Protein(eval(ifile), parsers[i-1])
                    self._combined_files[prot] = eval(f"self._args.combinecolor{i}")
                else:
                    break

            self.combine_pdb_files(self._combined_files)


        if self._args.spacefilling:
            self._tree = AtomAVLTree()
            self._root = None
            self.spacefilling_model()

        self._canvas.show()
        if sys.flags != 1:
            app.run()

    def vispy_setup(self):
        self._canvas = scene.SceneCanvas(keys="interactive",
                                         title=self._protein.name(), show=True,
                                         size=(800, 600))

        self._view                 = self._canvas.central_widget.add_view()
        self._view.camera.fov      = 100
        self._view.camera.distance = 1000

        self._view.camera = "turntable"
        ax_post = np.array([[-1, 0, 0],
                            [1, 0, 0],
                            [0, -1, 0],
                            [0, 1, 0],
                            [0, 0, -1],
                            [0, 0, 1]])

        self._axis = scene.visuals.XYZAxis(pos=ax_post)
        self._view.add(self._axis)

    def centralize_coords(self, coords: list, central_vec:list=[]) -> tuple:
        sum_x, sum_y, sum_z = 0, 0, 0
        for c in coords:
            sum_x += float(c[0])
            sum_y += float(c[1])
            sum_z += float(c[2])

        n = len(coords)
        central_vec = [sum_x / n, sum_y / n, sum_z / n]

        new_coords = list()
        for c in coords:
            coord_set = c
            for i in range(3):
                coord_set[i] = c[i] - central_vec[i]
            new_coords.append(coord_set)

        return (new_coords, central_vec)

    def create_vertec_color_map(self, verts:list) -> list:
        color_map = []
        for v in verts:
            dist = round(vc.calc_vec_distance(v.tolist(), [0, 0, 0]))
            try:
                atoms = self._tree.find(dist, self._root)._atom_list
                smallest_dist = vc.calc_vec_distance(v.tolist(), atoms[0][:3])
                color = atoms[0][3]
                for i in range(1, len(atoms)):
                    dist = vc.calc_vec_distance(v.tolist(), atoms[i][:3])
                    if dist < smallest_dist:
                        smallest_dist = dist
                        color = atoms[i][3]

                col = [col / 255 for col in color]
                color_map.append(col)
            except AttributeError:
                col = [col / 255 for col in [144, 144, 144]]
                color_map.append(col)

        return color_map

    def create_face_mesh(self, coords:list, colors:list, spec_color:list=None) -> object:
        pcd        = o3d.geometry.PointCloud()
        pcd.points = o3d.utility.Vector3dVector(coords)
        pcd.colors = o3d.utility.Vector3dVector(colors)
        pcd.estimate_normals()
        pcd.orient_normals_towards_camera_location(pcd.get_center())

        mesh  = o3d.geometry.TriangleMesh.create_from_point_cloud_poisson(pcd,depth=10, width=0, scale=1.1, linear_fit=True, n_threads=-1)
        verts = np.asarray(mesh[0].vertices)
        faces = np.asarray(mesh[0].triangles)

        if self._args.color != "none":
            vert_colors = self.create_vertec_color_map(verts)
            meshdata = geometry.MeshData(vertices=verts, faces=faces, vertex_colors=vert_colors)
        elif spec_color != None:
            meshdata = geometry.MeshData(vertices=verts, faces=faces, vertex_colors=spec_color*len(verts))
        else:
            meshdata = geometry.MeshData(vertices=verts, faces=faces)

        mesh = scene.visuals.Mesh(meshdata=meshdata, shading='smooth')
        self._view.add(mesh)


    def spacefilling_model(self):
        self._protein.atoms().pop(1)

        all_coords = []
        all_colors = []

        for atom_info in self._protein.atoms():
            atom      = self._protein.atoms()[atom_info].split()[1][0]
            atom_info = self._protein.atoms()[atom_info].split()

            last_idx = 7 if atom_info[7] == "1.00" else 8
            first_idx = last_idx-3

            try:
                coords = [float(c) for c in atom_info[first_idx:last_idx]]
            except ValueError:
                coords = [float(c) for c in atom_info[first_idx:last_idx-1]]
                coords.append(float(atom_info[last_idx-1][:5]))

            if self._args.color == "atom":
                atom_data = self._protein.atom_attrs(atom)
                col       = [int(atom_data["cpkHexColor"][i:i+2], 16) for i in (0, 2, 4)]#transform HEX-Colors to RGB
                all_colors.append(col)

            elif self._args.color == "temp":
                all_colors.append(float(atom_info[last_idx+2]))

            #self._root = self._tree.insert(self._root, [*coords, col])

            all_coords.append(coords)
        all_coords = self.centralize_coords(all_coords)
        for coords, col in zip(all_coords[0], all_colors):
            self._root = self._tree.insert(self._root, [*coords, col])

        self.create_face_mesh(all_coords[0], all_colors)

    def combine_pdb_files(self, coords_colors:dict) -> None:

            color_dict = {
                "orange": "#f35f1b",
                "purple": "#a775fa",
                "blue":   "#5db5f8",
                "green":  "#a2e625",
                "yellow": "#ffd966",
                "red":    "#d32821"
            }

            centered_vec = []
            all_coords   = []
            for key in coords_colors:
                try:
                    color = color_dict[coords_colors[key]]
                except ValueError:
                    print(f"Color {coords_colors[key]} isn't supported! Possible colors: {color_dict.keys()}")
                    sys.exit()

                for atom_info in key.atoms():
                    atom_info = key.atoms()[atom_info].split()

                    last_idx  = 8 if atom_info[7] == "1.00" else 8
                    first_idx = last_idx - 3

                    try:
                        coords = [float(c) for c in
                                  atom_info[first_idx:last_idx]]
                    except ValueError:
                        coords = [float(c) for c in
                                  atom_info[first_idx:last_idx - 1]]
                        coords.append(float(atom_info[last_idx - 1][:5]))
                    all_coords.append(coords)

                centered_coord, centered_vec = self.centralize_coords(all_coords, centered_vec)

                for coord in centered_coord:
                    if len(coord) == 3:
                        a = scene.visuals.Sphere(radius=2, color=color, shading='smooth')
                        a.transform = STTransform(translate=coord.tolist())

                        self._view.add(a)

    def ribbon_model(self):
        pass


def call_parser_and_renderer(file:str, args:object) -> None:
    file_end = file.split(".")[-1]

    try:
        molecule_parsers = {
            "xyz": "Parser.XYZ",
            "sdf": "Parser.SDF"
        }

        protein_parsers = {
            "pdb": "Parser.PDB"
        }

        if file_end in molecule_parsers.keys():
            parser = eval(f"{molecule_parsers[file_end]}('{file}')")
            MoleculeRenderer(file, args, parser)
        else:
            parsers = [eval(f"{protein_parsers[file_end]}('{file}')")]
            if args.combine:
                for i in range(2, 4):
                    ifile = eval(f"args.inputfile{i}")

                    if ifile == None: break
                    parser = eval(f"{protein_parsers[file_end]}('{ifile}')")
                    parsers.append(parser)
            ProteinRenderer(file, args, parsers)

    except Exception as e:
        if type(e) == KeyError:
            print(e)
            sys.stdout.write(f"File type {file_end} isn't supported\n")
        else:
            sys.stderr.write(str(traceback.print_exc()))


def argparser() -> object:
    ap = argparse.ArgumentParser("Molecule viewer for small molecules.")
    ap_gr = ap.add_mutually_exclusive_group(required=True)

    ap_gr.add_argument("-pc", "--pubchem", type=str,
                    help="Molecule structure, name or pubchem id")
    ap_gr.add_argument("-i", "--inputfile", type=str,
                    help="Inputfile with atom coordinates.")

    ap.add_argument("-s", "--sticky", default=False, action="store_true",
                     help="Changes the rendering to a sticky one."),
    ap.add_argument("-2d", "--planar_view", default=False, action="store_true",
                    help="Creates a 2d view of a molecule.")
    ap.add_argument("-c", "--color", type=str, default="none",
                    help="Sets the color scheme for the protein spacefilling model. " \
                         "Options: [atom: regions are colored by atom colors,]")

    ap.add_argument("-space", "--spacefilling", default=False, action="store_true",
                    help="Creates a spacefilling model of a protein")

    ap.add_argument("-comb", "--combine", default=False, action="store_true",
                    help="Enables adding of multiple files for cobination")
    ap.add_argument("-i2", "--inputfile2", type=str,
                       help="Inputfile2 with atom coordinates.")
    ap.add_argument("-i3", "--inputfile3", type=str,
                       help="Inputfile3 with atom coordinates.")
    ap.add_argument("-combc1", "--combinecolor1", type=str, help="color1 for combined files")
    ap.add_argument("-combc2", "--combinecolor2", type=str, help="color2 for combined files")
    ap.add_argument("-combc3", "--combinecolor3", type=str, help="color3 for combined files")

    return ap.parse_args()

if __name__ == "__main__":
    ap        = argparser()
    inputfile = pubchem_api_call(ap.pubchem)._file_name if ap.pubchem != None else ap.inputfile

    if ap.planar_view and sys.platform != "linux":
        sys.strout.write(f"{__name__}: The planar view(2d) is currently only working on a Linux based system.\n")
        sys.exit()
    
    call_parser_and_renderer(inputfile, ap)
