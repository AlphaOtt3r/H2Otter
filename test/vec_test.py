#!/usr/bin/env python3

import sys
sys.path.append("../")

import unittest
import vector_calcs
import math

class TestVecCalcs(unittest.TestCase):
    def test_vec_dis(self):
        v, w = [2, 3, 4, 2], [1, -2, 1, 3]
        dis  = 6.0

        self.assertEqual(vector_calcs.calc_vec_distance(v, w), dis)

    def test_vec_center(self):
        v, w = [4, 2], [4, -4]
        center = [4, -1]

        self.assertEqual(vector_calcs.calc_vec_center(v, w), center)

    def test_vec_angle(self):
        v, w  = [3, 4, 0], [4, 4, 2]
        angle = math.degrees(math.acos(14/15))

        self.assertEqual(vector_calcs.calc_vec_angel(v, w), angle)

    def test_vec_direction(self):
        v, w = [4, 10], [2, 1]
        dir  = 180+(math.degrees(math.atan((w[1]-v[1])/(w[0]-v[0]))))
        directions = {0: "N", 90: "E", 180: "S", 270: "W", 360: "N"}

        for i, key in enumerate(directions):
            if dir >=key:
                if dir == key:
                    comp = directions[key]
                    break
                if dir < list(directions)[i+1]:
                    comp = f"{directions[key]}/{directions[list(directions)[i+1]]}"
                    break
                else: continue

        vc_dir = vector_calcs.calc_vec_direction(v, w)

        self.assertEqual(vc_dir[0], dir)
        self.assertEqual(vc_dir[1], comp)


if __name__ == "__main__":
    unittest.main()