import sys
sys.path.append("../src")

import unittest
from datastructures import *
import vector_calcs as vc

class TestAVLTree(unittest.TestCase):
    def test_insert(self):
        atom_list     = [
            [1, 2, 3, [0.2, 0.3, 0.5]],
            [2, 5, 3, [0.2, 0.3, 0.5]],
            [6, 7, 8, [0.2, 0.3, 0.5]],
            [1, 2, 3, [0, 0.3, 0.7]],
            [3, 7, 8, [0.7, 0.2, 0.1]],
            [0.5, 0.6, 1, [0.7, 0.2, 0.1]]
        ]

        preoder_list = [
            [3.7416573867739413],
            [3.7416573867739413, 6.164414002968976],
            [6.164414002968976, 3.7416573867739413, 12.206555615733702],
            [6.164414002968976, 3.7416573867739413, 12.206555615733702],
            [6.164414002968976, 3.7416573867739413, 12.206555615733702, 11.045361017187261],
            [6.164414002968976, 3.7416573867739413, 1.268857754044952, 12.206555615733702, 11.045361017187261]
        ]

        tree = AtomAVLTree()
        root = None

        for i in range(len(atom_list)):
            root = tree.insert(root, atom_list[i])
            self.assertEqual(preoder_list[i], tree.preorder(root))

    def test_delete(self):
        pass

    def test_find(self):
        atom_list = [
            [1, 2, 3, [0.2, 0.3, 0.5]],
            [2, 5, 3, [0.2, 0.3, 0.5]],
            [6, 7, 8, [0.2, 0.3, 0.5]],
            [1, 2, 3, [0, 0.3, 0.7]],
            [3, 7, 8, [0.7, 0.2, 0.1]],
            [0.5, 0.6, 1, [0.7, 0.2, 0.1]]
        ]

        tree = AtomAVLTree()
        root = None

        for atom in atom_list:
            root = tree.insert(root, atom)

        for atom in atom_list:
            key  = vc.calc_vec_distance(atom[:3], [0, 0, 0])
            node = tree.find(key, root)
            self.assertEqual(node._value, key)


if __name__ == "__main__":
    unittest.main()