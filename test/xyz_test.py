#!/usr/bin/env python3

import sys
sys.path.append("../")

import unittest
import FileParser

class TestXYZFileParser(unittest.TestCase):
    def __init__(self, *args, **kw):
        unittest.TestCase.__init__(self, *args, *kw)

        test_file_path = "../data/xyz/h2o.xyz"
        self._parser   = FileParser.XYZ(test_file_path)

        with open(test_file_path, "r") as test_file:
            lines = test_file.readlines()

            self._atom_count = lines[0].strip()
            self._name       = lines[1].strip()

            self._atoms_a_coords = [line.split() for line in lines[2:]]

    def test_atoms_a_coords(self):
        atoms      = self._parser.atoms()
        atom_lines = [atoms[atom] for atom in atoms]
        self.assertEqual(self._atoms_a_coords, atom_lines)

    def test_name(self):
        self._parser_name = self._parser.name()
        self.assertEqual(self._name, self._parser_name)

    def test_atom_count(self):
        self._parser_atom_count = self._parser.atom_count()
        self.assertEqual(int(self._atom_count), self._parser_atom_count)


if __name__ == "__main__":
    unittest.main()