# H2Otter
A simple molcule viewer for smaller molecules. Made with Python3 and VisPy for visualisation.
I'm working on this project beside Uni and that's the reason for the slow progress. 
This one of the biggest project I've had worked on.
If you want to contribute, make some feature suggestions, report bugs or just want to contact me, here is my email address: AlphaOtt3r@gmail.com

## Table of Contents

- [Installation](#installation)
- [Features](#features)
- [Usage](#usage)

## Installation
Supported python versions: python3.6 - python3.9
> Install this program using the following commands
```
git clone https://gitlab.com/AlphaOtt3r/H2Otter.git
pip install -r requirements.txt
```
> Currently there is a linux exclusive feature (the 2D view)
> This feature uses Rscript, the installation is depends on the distro. Some examples:
```
Ubuntu:
sudo apt install r-base

Fedora:
sudo dnf install R
```

## Features
- Planar view of small molecules.

![2D view](https://gitlab.com/AlphaOtt3r/H2Otter/-/raw/master/img/molecule_2d/hydrogen_peroxide_staggered.png)

- 3D view of smaller molecules, there are currently two modes. The sticky and stick and balls visualization.
![3D view](https://gitlab.com/AlphaOtt3r/H2Otter/-/raw/master/img/molecule_3d/caffeine_sdf_stick_and_ball.png)
![3D view](https://gitlab.com/AlphaOtt3r/H2Otter/-/raw/master/img/molecule_3d/caffeine_sdf_stick_and_ball.png)

- 3D protein spacefilling model 
![2D view](https://gitlab.com/AlphaOtt3r/H2Otter/-/raw/master/img/molecule_3d/GFP_Spacefilling_model_3d.png)


As you can see in the images above the rendering of multiple bonds isn't perfect yet.
More features will be added in the future, like the 3d visualization of proteins.

Supported file types:

| Types  | Implementation  |
| -----  | -----  |
| XYZ | Complete support | 
| SDF | Complete support | 
| ZMAT | Will be added | 
| PDB  | Spacefilling model |

## Usage
````
python Moleculeviewer [options]
````

| Option  | Description  | Example |
| -----  | -----  | -----  |
| -i / --inputfile | Inputfile with atom coordinates. | ```python Moleculeviewer -i data/xyz/h2o.xyz``` |
| -s / --sticky| Changes the rendering to a sticky one. | ```python Moleculeviewer -i data/xyz/h2o.xyz -s```|
| -pc / --pubchem | Searches the  pubchem platform by Molecule structure, name or pubchem id and downloads the file |```python Moleculeviewer -pc ethanol``` |
| -2d / --planar_view  | (Only Linux support) Creates a 2d view of a molecule.  | ```python Moleculeviewer -2d data/xyz/h2o.xyz``` |
| -space / --spacefilling, -c /--color  | Option for PDB files, creates atom colored spacefilling model| ```python Moleculeviewer -i data/sdf/2Y0G.pdb -space -c atom``` |
