#Author: AlphaOtt3r
#Fundamental constants
#https://www.nist.gov/pml/fundamental-physical-constants


#Avogadro constant in an physical constant, which defines the constituent particles(N) in a amount of substance(n). Formula: N/n = Na
avogadro_number = 6.02214179e23 #(30)mol⁻¹




#Mass constans
#Dalton(Da) = u; Dalton is a unit for atomic mass

#Dalton unit constant to convert Da into kg
dalton_constant = 1.660538782e-27 #(83) u -> kg

#Mass of proton in u
Mp = 1.007276466621 #(53) u

#Mass of neuron mass in u
Mn = 1.00866491595 #(49) u 

#Mass of electron in u
Me = 5.48577990946e-4 #(22) u
