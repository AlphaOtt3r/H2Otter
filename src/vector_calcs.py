#!/usr/bin/env python3

import math
import sys

def calc_vec_distance(v:list, w:list) -> float:
    """
    v, w: list of coords
    """

    if len(v) != len(w):
        sys.stderr.write(f"{__name__}: vector lengths aren't euqal!\n")
        exit(1)

    dis = 0
    for i in range(len(v)):
        dis += (float(v[i])-float(w[i]))**2

    return math.sqrt(dis)

def calc_vec_center(v:list, w:list) -> list:
    """
    v, w: list of coords
    """

    if len(v) != len(w):
        sys.stderr.write(f"{__name__}: vector lengths aren't euqal!\n")
        exit(1)

    center = []
    for i in range(len(v)):
        center.append((float(v[i]) + float(w[i]))/2)

    return center

def calc_vec_magnitude(v:list):
    """
        v: list of coords
    """

    if len(v) == 0:
        sys.stderr.write(f"{__name__}: vector is to small!\n")
        exit(1)

    mag = 0
    for i in range(len(v)):
        mag += float(v[i])**2

    return math.sqrt(mag)

def calc_vec_angel(v:list, w:list):
    """
    v, w: list of coords
    """

    if len(v) != len(w):
        sys.stderr.write(f"{__name__}: vector lengths aren't euqal!\n")
        exit(1)

    dot_product = 0

    for i in range(len(v)):
        dot_product += float(v[i]) * float(w[i])

    v_mag = calc_vec_magnitude(v)
    w_mag = calc_vec_magnitude(w)
    angle = math.acos(dot_product/(v_mag*w_mag))

    return math.degrees(angle)

def calc_vec_direction(v:list, w:list) -> str:
    """
    v, w: list of coords, [x, y, z]
    """

    if len(v) != len(w):
        sys.stderr.write(f"{__name__}: y and x coord list lengths aren't euqal!\n")
        exit(1)

    x_coords, y_coords = [v[0], w[0]], [v[1], w[1]]

    directions = {0: "N", 90: "E", 180: "S", 270: "W", 360: "N"}
    x,y        = 0, 0

    for xi, yi in zip(x_coords[::-1], y_coords[::-1]):
        if x == 0 and y == 0:
            x, y = xi, yi
        else:
            x -= xi
            y -= yi

    dir = math.atan(y/x)
    deg = (180+math.degrees(dir))

    for i, key in enumerate(directions):
        if deg >= key:
            if deg == key:
                comp = directions[key]
                break
            if deg < list(directions)[i + 1]:
                comp = f"{directions[key]}/{directions[list(directions)[i + 1]]}"
                break

            else: continue

    return deg, comp7