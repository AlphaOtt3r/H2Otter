#This file creates an 2d plot from given coords and symbols, which
#are given by a python script

args <- commandArgs(trailingOnly = TRUE)

atom_amount <- as.numeric(args[1])
bond_count  <- as.numeric(args[2])

bond_idx <- bond_count + 2
bonds    <- c(as.numeric(args[3:bond_idx]))
x_bonds  <- bonds[seq_along(bonds) %% 2 > 0]
y_bonds  <- bonds[1:(bond_idx/2)*2]
y_bonds  <- y_bonds[1:length(y_bonds)-1]

atoms_idx <- bond_idx+atom_amount
atoms     <- args[bond_idx+1:atom_amount]

atom_coords <- as.numeric(args[atoms_idx+1:(atom_amount*2)])
hexcols     <- args[atoms_idx+(atom_amount*2)+1:atom_amount]

filename <- args[atoms_idx+(atom_amount*2)+atom_amount+1]

png(filename, width=1000, height=800, units="px")
par(bg="gray10")
plot(rnorm(1),xlim=c(-3-(atom_amount/5)),5+(atom_amount/5)),ylim=c(-3-(atom_amount/5)),5+(atom_amount/5)),type="n",axes=F,xlab="",ylab="")

count <- 0
for(i in 1:length(atoms)){
  text(atom_coords[count+1:2][1],atom_coords[count+1:2][2], atoms[i],
       cex=2, col=paste("#", hexcols[i], sep=""))
  lines(x_bonds[count+1:2], y_bonds[count+1:2], col="white")

  count <- count +2
}

dev.off()
