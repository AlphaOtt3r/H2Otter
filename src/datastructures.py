#!/usr/bin/env python3
import vector_calcs as vc
import numpy as np

class AtomAVLTreeNode(object):
    def __init__(self, value:float, atom:list) -> None:
        """
        :param value: numeric distance of atom to [0, 0, 0]
        :param atom:  A list of atom attrs [X, Y, Z, [R, G, B]]
        """

        self._value     = value
        self._atom_list = list()
        self._left      = None
        self._right     = None
        self._height    = 1

        self._atom_list.append(atom)

class AtomAVLTree(object):
    def insert(self, root:object, atom:list) -> object:
        """
        :param root: the root of the AVL tree as Node object
        :param atom: A list of atom attrs [X, Y, Z, [R, G, B]]
        :return: new root object of the AVL tree as Node object
        """

        center = [0, 0, 0]
        key    = vc.calc_vec_distance(atom[:3], [0, 0, 0])

        if not root:
            return AtomAVLTreeNode(key, atom)
        if key < root._value:
            root._left = self.insert(root._left, atom)
        elif key > root._value:
            root._right = self.insert(root._right, atom)
        else:
            root._atom_list.append(atom)
            return root

        root._height = 1 + max(self.get_height(root._left), self.get_height(root._right))
        balance      = self.get_balance(root)

        if balance > 1 and key < root._left._value:
            return self.rotate_right(root)

        elif balance < -1 and key > root._right._value:
            return self.rotate_left(root)

        elif balance > 1 and key > root._left._value:
            root._left = self.rotate_left(root._left)
            return self.rotate_right(root)

        elif balance < -1 and key < root._right._value:
            root._right = self.rotate_right(root._right)
            return self.rotate_left(root)

        return root

    def delete(self):
        pass

    def find(self, key:float, node:object) -> object:
        """
        :param key: The key for which will be searched
        :param node: A node of the AVL tree
        :return: A node object
        """

        if not node:
            return
        if key < node._value:
            return self.find(key, node._left)
        elif key > node._value:
            return self.find(key, node._right)
        else:
            return node


    def get_max_width(self, root:object) -> int:
        """
        :param root: the root of the AVL tree as Node object
        :return: maximal width of the AVL tree as int
        """
        max_width = 0
        h = self.get_height(root)

        for i in range(1, h + 1):
            width = self.get_width(root, i)
            if width > max_width:
                max_width = width

        return max_width

    def get_width(self, node:object, level:int) -> int:
        """
        :param node: A node of the AVL tree
        :param level: the level of the node
        :return: The width of the current node
        """

        if node is None:
            return 0
        elif level == 1:
            return 1
        elif level > 1:
            if (node._left and node._right) is None:
                return  0
            elif (node._left or node._right) is None:
                if node._left is None:
                    return self.get_height(node._right)
                elif node._right is None:
                    return self.get_height(node._left)
            else:
                return (self.get_width(node._left, level-1)) + \
                        (self.get_width(node._right, level-1))

    def get_height(self, node:object) -> int:
        """
        :param node: A node of the AVL tree
        :return: the height of the current node
        """

        if not node:
            return 0
        else:
            return node._height

    def get_max_height(self, node:object) -> int:
        """
        :param node: A node of the AVL tree
        :return: maximal height of the AVL tree as int
        """

        max = 0
        h   = self.get_height(node)

        for i in range(1, h+1):
            width = self.get_width(node, h+1)
            if width > max:
                max = width

        return max

    def get_balance(self, node:object) -> int:
        """
        :param node: A node of the AVL tree
        :return: the balance factor of a node as int
        """

        if not node:
            return 0
        else:
            return self.get_height(node._left) - self.get_height(node._right)

    def rotate_left(self, node:object) -> object:
        """
        :param node: A node of the AVL tree
        :return: rotated node
        """

        y   = node._right
        tmp = y._left

        y._left     = node
        node._right = tmp

        node._height = 1 + max(self.get_height(node._left),
                              self.get_height(node._right))
        y._height    = 1 + max(self.get_height(y._left),
                              self.get_height(y._right))

        return y

    def rotate_right(self, node:object) -> object:
        """
        :param node: A node of the AVL tree
        :return: rotated node
        """

        y   = node._left
        tmp = y._right

        y._right   = node
        node._left = tmp

        node._height = 1 + max(self.get_height(node._left),
                              self.get_height(node._right))
        y._height = 1 + max(self.get_height(y._left),
                           self.get_height(y._right))

        return y

    def preorder(self, root:object) -> list:
        """
        :param root: A node of the AVL tree
        :return: the preorder of the AVL tree as list
        """
        preorder = []
        if not root:
            return preorder

        preorder.append(root._value)
        preorder.extend(self.preorder(root._left))
        preorder.extend(self.preorder(root._right))

        return preorder
