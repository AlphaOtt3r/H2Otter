#!/usr/bin/env python3

import sys
import re
from . import vector_calcs as vc

class TSV:
    def __init__(self, filename:str, sort_by:str=None) -> None:
        """
        filename: a string of the file path
        sort_by: header key, which will be used for the sorted dict
        """

        try:
            stream = open(filename, "r")
        except IOError:
            sys.stderr.write(f"{__name__}: can't open file {filename}!")
            exit(1)

        self._sort_by = sort_by
        self._tsv_data = self.get_tsv(stream.readlines())

        stream.close()

    def get_tsv(self, stream):
        headers = stream[0].split("\t")
        content = dict()

        if self._sort_by:
            if not self._sort_by in headers:
                sys.stderr.write("Sort key wasn't found! Using default....")
        else:
            self._sort_by = headers[0]

        value_idx     = headers.index(self._sort_by)
        for line in stream:
            values    = line.split("\t")
            content[values[value_idx]] = dict()

            for header, value in zip(headers, values):
                content[values[value_idx]][header] = value

        return content

    def data(self):
        return self._tsv_data


class Data_file:
    def __init__(self, filename:str):
        self._name       = None
        self._atom_count = None
        self._atom_dict  = dict()
        self._bond_dict  = dict()

    def atoms(self) -> dict:
        return self._atom_dict

    def name(self) -> str:
        return self._name

    def atom_count(self) -> int:
        return self._atom_count

    def additional_info(self):
        return "None"

    def bonds(self) -> dict:
        return self._bond_dict

    def __str__(self):
        return "None"


class XYZ(Data_file):
    def __init__(self, filename:str) -> None:
        super().__init__(filename)

        try:
            stream = open(filename, "r")
        except IOError:
            sys.stderr.write(f"{__name__}: can't open file {filename}\n")
            exit(1)

        for id, line in enumerate(stream):
            line = line.rstrip()

            if id == 0:
                self._atom_count = int(line)

            elif id == 1:
                if line != "": self._name = line

            else:
                atom = re.compile(r"(-?\d*\d.\d*|\w*\w)").findall(line)
                self._atom_dict[id-2] = list()
                self._atom_dict[id-2].append(atom[0])
                self._atom_dict[id-2].append(atom[1])
                self._atom_dict[id-2].append(atom[2])
                self._atom_dict[id-2].append(atom[3])
        stream.close()

        self._bond_dict = self.create_bonds()

    def create_bonds(self):
        coord_combs = dict()
        atoms       = self.atoms()
        tmp_l       = []

        for j, atom_f in enumerate(atoms):
            atom_f   = atoms[atom_f]
            coords_f = list(map(float, atom_f[1:]))

            for i, atom_s in enumerate(atoms):
                atom_s   = atoms[atom_s]
                coords_s = list(map(float, atom_s[1:]))

                if coords_f != coords_s:
                    dis = vc.calc_vec_distance(coords_f, coords_s)

                    if "H" == atom_s[0] or "H" == atom_f[0]:
                        bd = 260
                    else: bd = 400

                    if dis < (bd/200):
                        a_name = f"{atom_f[0]}-{atom_s[0]},{i}{j}"
                        comb      = [atom_f[1:], atom_s[1:]]

                        if len(coord_combs) == 0:
                            coord_combs[a_name]            = dict()
                            coord_combs[a_name]["coords"]  = comb
                            coord_combs[a_name]["type"]    = "1"
                            tmp_l.append(comb)

                        else:
                            cd = a_name.split(",")[0].split("-")
                            nl = [[a.split(",")[0].split("-")] for a in coord_combs.keys()]

                            if cd or cd[::-1] in nl:
                                if comb and comb[::-1] not in tmp_l:
                                    coord_combs[a_name]            = dict()
                                    coord_combs[a_name]["coords"] = comb
                                    coord_combs[a_name]["type"] = "1"
                                    tmp_l.append(comb)

        return coord_combs

    def __str__(self) -> str:
        atoms    = self.atoms()
        atom_str = "\n".join(["\t".join(atoms[i]) for i in range(len(atoms))])
        file_str = f"{self.name()}\n{self.atom_count()}\n{atom_str}"

        return str(file_str)


class SDF(Data_file):
    def __init__(self, filename: str) -> None:
        super().__init__(filename)

        try:
            stream = open(filename, "r")
        except IOError:
            sys.stderr.write(f"{__name__}: can't open file {filename}\n")
            exit(1)

        lines = stream.readlines()

        #File blocks
        self._header     = "-".join(lines[:3])
        count_line       = lines[3].split()
        atom_block_lines = 4+int(count_line[0])
        bond_block_lines = int(count_line[1])+atom_block_lines
        atom_block       = lines[4:atom_block_lines]
        bond_block       = lines[atom_block_lines:bond_block_lines]
        desc             = [l for l in lines[bond_block_lines:] if l != "$$$$"]

        self._name = filename.split("/")[-1].split(".")[0]

        try:
            self._name += f' -'  + desc[desc.index("> <PUBCHEM_MOLECULAR_FORMULA>\n") + 1]
        except ValueError: pass

        self._atom_count       = count_line[0]
        self._atom_dict        = dict()
        self._bond_dict        = dict()
        self._additional_infos = desc

        for i, a in enumerate(atom_block):
            atom = re.compile(r"(-?\d*\d.\d*|\w*\w)").findall(a)
            self._atom_dict[i+1] = list()
            self._atom_dict[i+1].append(atom[3])
            self._atom_dict[i+1].append(atom[0])
            self._atom_dict[i+1].append(atom[1])
            self._atom_dict[i+1].append(atom[2])

        for i, a in enumerate(bond_block):
            bond_line = a.split()
            f_atom    = self._atom_dict[int(bond_line[0])]
            s_atom    = self._atom_dict[int(bond_line[1])]
            comb      = [f_atom[1:], s_atom[1:]]
            self._bond_dict[f"{f_atom[0]}-{s_atom[0]},{i}"] = dict()
            self._bond_dict[f"{f_atom[0]}-{s_atom[0]},{i}"]["coords"] = comb
            self._bond_dict[f"{f_atom[0]}-{s_atom[0]},{i}"]["type"]   = bond_line[2]

    def additional_info(self):
        return self._additional_infos

    def __str__(self):
        atoms = self.atoms()
        atom_str = "\n".join(["\t".join(atoms[i+1]) for i in range(len(atoms))])
        file_str = f"{self.name()}\n{self.atom_count()}\n{atom_str}"

        return f"Header: {self._header} \n Molecule:\n {file_str} \n\n " \
               f"Additional Info: \n {''.join(self._additional_infos)}"


class PDB(Data_file):
    def __init__(self, filename:str):
        super().__init__(filename)

        try:
            stream = open(filename, "r")
        except IOError:
            sys.stderr.write(f"{__name__}: can't open file {filename}\n")
            sys.exit()

        file     = "\n".join(stream.readlines())
        pdb_file = {"HEADER": [], "TITLE": [], "COMPND": [], "SOURCE": [],
                    "AUTHOR": [], "REVDAT": [], "REMARK": [], "SPRSDE": [],
                    "SEQRES": [], "FTNOTE": [], "HET": [], "FORMUL": [], 
                    "HELIX": [], "SHEET": [], "TURN": [], "CRYST1": [], 
                    "ORIG": [], "SCALE": [], "ATOM": [], "HETATM": [], 
                    "CONECT": [], "MASTER": [], "END": []}
        
        for content in pdb_file.keys():
            lines = re.compile(fr"^{content}\s(.*)", re.MULTILINE).findall(file)
            #lines = re.search(fr"^{content}\s(.*)", file, re.MULTILINE)
            pdb_file[content].extend([" ".join(l.split()) for l in lines]) 
        
        self._name       = "-".join(pdb_file["TITLE"])
        self._atom_dict  = dict()

        for i, a in enumerate(pdb_file["ATOM"]):
            self._atom_dict[i+1] = pdb_file["ATOM"][i]

        self._atom_count = len(pdb_file["ATOM"])


    def __str__(self) -> str:
        return self._name
