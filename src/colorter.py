#Author: AlphaOtt3r
#Email: AlphaOtt3r@gmail.com
#Desc.: Module for colored terminal Output
#Color-table: https://en.wikipedia.org/wiki/ANSI_escape_code

import platform

reset_colors = "0"

def colorter(msg, fg="white", bg="black", dec="normal"):
    os = platform.system()

    if os == "Linux":
        linux_fg_color_dic = {"black": "30", "red": "31", "green": "32", "orange": "33","brown": "33", "blue": "34", "purple": "35", "cyan": "36", "light_grey":"37"}
        linux_fg_light_dic = {"light_blue": "94", "light_red": "91", "light_purple": "95", "light_cyan": "96", "white": "97", "yellow": "93", "dark_grey": "90", "light_green": "92"}
        linux_fg_colors_dic = {**linux_fg_color_dic, **linux_fg_light_dic}


        linux_bg_color_dic = {"black": "40", "red": "41", "green": "42", "orange": "43", "brown":"43", "blue": "44", "purple": "45", "cyan": "46", "light_grey":"47"}
       	linux_bg_light_dic = {"dark_grey": "100", "light_red": "101", "yellow": "103", "light_blue": "104", "light_purple": "105", "light_cyan": "106", "white": "107", "light_green": "102"}
        linux_bg_colors_dic = {**linux_bg_color_dic, **linux_bg_light_dic}

        linux_decorations_dic = {"under": "4", "bold": "1", "normal": "0"}
	
        fg_color_code = linux_fg_colors_dic[fg]
        bg_color_code = linux_bg_colors_dic[bg]
        dec_code      = linux_decorations_dic[dec]
 

        colored_output = f"\033[{bg_color_code};{dec_code};{fg_color_code}m{msg}\033[{reset_colors}m"
        return colored_output
    
    else:   print("Module is only Linux based systems.")