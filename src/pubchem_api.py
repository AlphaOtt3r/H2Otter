import requests
import sys, re

class pubchem_api_call:
    def __init__(self, search_term:str):
        self._name = search_term
        self._base_url     = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/"
        self._file_dir     = "data/sdf/"
        self._file_content = None
        self._file_name    = None

        try:
            self._search_term = int(search_term)
            self.request_molecule_by_cid()
        except ValueError:
            self._name        = search_term
            self._search_term = search_term.replace("_", " ")
            self.request_molecule_by_name()

    def request_molecule_by_name(self):
        url = self._base_url + f"compound/name/{self._search_term}/cids/TXT"
        req = requests.get(url)

        if req.status_code != 200:
            sys.stderr.write(f"{__name__}: can't request {self._search_term}\n")
            exit(1)

        self._search_term = req.text.strip()
        self.request_molecule_by_cid()

    def request_molecule_by_cid(self):
        url = f"{self._base_url}compound/cid/{self._search_term}/sdf?record_type=3d"
        req = requests.get(url)

        if req.status_code != 200:
            sys.stderr.write(f"{__name__}: can't request {self._search_term}\n")
            exit(1)

        self._file_content = req.content
        self.save_file()

    def save_file(self):
        self._file_name = self._file_dir+f"{self._name}.sdf"
        with open(self._file_name, "wb+") as sdf_file:
            sdf_file.write(self._file_content)

    def file_content(self):
        return self._file_content

    def file_name(self):
        return self._file_name

    def molecule_structure(self) -> str:
        file = self._file_content.decode()
        return re.compile("> <PUBCHEM_MOLECULAR_FORMULA>\n(\w*)").findall(file)[0]

    def name(self) -> str:
        return self._name